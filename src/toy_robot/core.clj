(ns toy-robot.core
  (:require [clojure.string :as string])
  (:gen-class))

;; The state of the robot.
;; Represented by a vector of [x y direction]
;;   where
;;   x is the horizontal position of robot
;;   y is the vertical position of robot
;;   direction is the direction that the robot is facing
(def robot-state (atom nil))

(def grid-size [5 5])


;; A table for conversion between directions and angles.
(def direction-tbl
  {:north 0
   :east 90
   :south 180
   :west 270})


(defn place
  "Place a robot at position [x y], facing <direction>."
  [state]
  {:pre [(contains? #{:north :east :south :west} (state 2))
         (<= 0 (state 0) (dec (grid-size 0)))
         (<= 0 (state 1) (dec (grid-size 1)))]}
  (reset! robot-state state))


(defn move
  "Function to move robot one cell forward in direction facing.
  It has pre- and post-conditions to prevent robot from falling out of grid.
  In the post-conditions, % refers to the function's return value."
  [state]
  {:pre [(<= 0 (state 0) (dec (grid-size 0)))
         (<= 0 (state 1) (dec (grid-size 1)))]
   :post [(<= 0 (% 0) (dec (grid-size 0)))
          (<= 0 (% 1) (dec (grid-size 1)))]}
  (condp = (state 2)
    :north (update-in state [1] inc)
    :east (update-in state [0] inc)
    :south (update-in state [1] dec)
    :west (update-in state [0] dec)))


;; Utility function.
(defn map-invert
  "Returns the map with the vals mapped to the keys."
  [m] (reduce (fn [m [k v]] (assoc m v k)) {} m))


(defn turn
  "Turn the robot by <angle> degrees."
  [state angle]
  (let [current-angle (direction-tbl (state 2))
        new-angle (+ current-angle angle)
        new-angle-adjusted (if (>= new-angle 360)
                             (- new-angle 360)
                             new-angle)
        new-direction ((map-invert direction-tbl) new-angle-adjusted)]
    (assoc state 2 new-direction)))


(defn turn-left
  [state]
  (turn state 270))


(defn turn-right
  [state]
  (turn state 90))


(defn when-robot-exists
  [state]
  (when @robot-state
    (reset! robot-state state)))

(defn report
  []
  (when @robot-state
    (println (str "> " @robot-state))
    true))


(defn input->command
  "Convert raw string input to command.
  A command is represented by a vector."
  [input]
  (vec (map string/lower-case
         (string/split input #"\s+|,\s+"))))


(defn execute
  [input]
  (let [command (input->command input)]
    (condp = (command 0)
      "place" (let [state [(read-string (command 1))
                           (read-string (command 2))
                           (keyword (command 3))]]
                (place state))
      "move" (when-robot-exists (move @robot-state))
      "left" (when-robot-exists (turn-left @robot-state))
      "right" (when-robot-exists (turn-right @robot-state))
      "report" (report)
      :invalid-command)))


(defn -main
  [& args]
  (println "> Robot ready, please enter your command:")
  (while true
    (try
      (let [result (execute (read-line))]
        (cond
          (= result :invalid-command) (println "> Command not recognised, try a valid command.")
          (= result nil) (println "> The robot has not been placed on tabletop yet.")
          :else (println "> Ok")))
      (catch AssertionError e
        (println "> Either the Robot has reached boundary, or the command is invalid. No actions taken."))
      (catch NullPointerException e
        (println "> The robot has not been placed on tabletop yet.")))))
