(ns toy-robot.core-test
  (:require [clojure.test :refer :all]
            [toy-robot.core :as rc]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]))


;; Properties of input
(def valid-inputs
  (gen/tuple
    (gen/choose 0 4)
    (gen/choose 0 4)
    (gen/elements [:north :east :south :west])))


;; Here we expect some AssertionError exceptions when the robot hits the boundary.
;; The pre- and post-conditions of rc/move prevents the robot from falling out of
;; the boundary.
(deftest move-fn-moves-forward-one-cell-in-grid
  (for [input (take 1000 (gen/sample-seq valid-inputs))]
    (or
      (is (thrown? AssertionError (rc/move input)))
      (let [output (rc/move input)]
        (and
          (condp = (input 2)
            :north (= (inc (input 1)) (output 1))
            :east (= (inc (input 0)) (output 0))
            :south (= (dec (input 1)) (output 1))
            :west (= (dec (input 0)) (output 0)))
          (<= 0 (output 0) (rc/grid-size 0))
          (<= 0 (output 1) (rc/grid-size 1)))))))


(defspec turn-left-fn-should-always-change-direction-to-the-left
  1000
  (prop/for-all [input valid-inputs]
    (let [input-direction (input 2)
          output (rc/turn-left input)
          output-direction (output 2)]
      (condp = input-direction
        :north (= output-direction :west)
        :east (= output-direction :north)
        :south (= output-direction :east)
        :west (= output-direction :south)))))


(defspec turn-right-fn-should-always-change-direction-to-the-right
  1000
  (prop/for-all [input valid-inputs]
    (let [input-direction (input 2)
          output (rc/turn-right input)
          output-direction (output 2)]
      (condp = input-direction
        :north (= output-direction :east)
        :east (= output-direction :south)
        :south (= output-direction :west)
        :west (= output-direction :north)))))